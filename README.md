# SpringBootDemo-OpenFlightsAPI

# OpenFlights API Specification
https://openflights.org/data.html

#Requirement
1. Use the Airline, Airport and Routes data from OpenFlights spec
2. Download and read the .dat files
3. Define a schema and store the data into an in-memory database
4. Provide API' for
    1. Flights flying into a specified airport
    2. Flights flyout out of a specified airport
    3. Flights between 2 specified airports

# OpenFlightsAPI-SpringBoot
SpringBoot application integrated with OpenFlights data to get flights information from airports, airlines and routes

# Steps to run the API
1. Clone the OpenFlightsAPI-SpringBoot and open the project "com.foxtel.spring.demo"
2. Run Application.java - main Spring boot application or run the build.gradle or run this command from Terminal "gradlew clean build"
3. data.sql should accept relative path of the .dat files, if the application fails to launch, try providing the absolute path of the .dat files. Some versions of H2 database have this bug
4. Once the application is running, make a note of the port(should be 8080 by default)

Run the API endpoint /flights/in/airporpt/{airportId} for flights flying in to the airport
Ex. http://localhost:8080/flights/in/airport/3361 should return the JSON response
```
[
    {
        "airportName": "Sydney Kingsford Smith International Airport",
        "airlineId": "1",
        "airlineName": "Private flight",
        "destinataionAirportId": "3361",
        "sourceAirportId": "3393",
        "active": "Y",
        "stops": null
    },
    {
        "airportName": "Sydney Kingsford Smith International Airport",
        "airlineId": "3",
        "airlineName": "1Time Airline",
        "destinataionAirportId": "3361",
        "sourceAirportId": "3393",
        "active": "Y",
        "stops": null
    },
    {
        "airportName": "Sydney Kingsford Smith International Airport",
        "airlineId": "10",
        "airlineName": "40-Mile Air",
        "destinataionAirportId": "3361",
        "sourceAirportId": "3393",
        "active": "Y",
        "stops": null
    },
    ...
]
```
Run the API endpoint /flights/out/airport/{airportId} for flights flying out from the airport
Ex. http://localhost:8080/flights/out/airport/3361 should return the JSON response
```
[
    {
        "airportName": "Sydney Kingsford Smith International Airport",
        "airlineId": "1",
        "airlineName": "Private flight",
        "destinataionAirportId": "3393",
        "sourceAirportId": "3361",
        "active": "Y",
        "stops": null
    },
    {
        "airportName": "Sydney Kingsford Smith International Airport",
        "airlineId": "3",
        "airlineName": "1Time Airline",
        "destinataionAirportId": "3393",
        "sourceAirportId": "3361",
        "active": "Y",
        "stops": null
    },
    {
        "airportName": "Sydney Kingsford Smith International Airport",
        "airlineId": "10",
        "airlineName": "40-Mile Air",
        "destinataionAirportId": "3393",
        "sourceAirportId": "3361",
        "active": "Y",
        "stops": null
    },
    ...
]
```
Run the API endpoint /flights/route/source/{sourceAirportId}/dest/{destinationAirportId} for flights flying between two airports
Ex. http://localhost:8080/flights/route/source/3361/dest/3339 should return the JSON response
```
[
    {
        "airportName": null,
        "airlineId": "1",
        "airlineName": "Private flight",
        "destinataionAirportId": "3339",
        "sourceAirportId": "3361",
        "active": "Y",
        "stops": "0"
    },
    {
        "airportName": null,
        "airlineId": "3",
        "airlineName": "1Time Airline",
        "destinataionAirportId": "3339",
        "sourceAirportId": "3361",
        "active": "Y",
        "stops": "0"
    },
    {
        "airportName": null,
        "airlineId": "10",
        "airlineName": "40-Mile Air",
        "destinataionAirportId": "3339",
        "sourceAirportId": "3361",
        "active": "Y",
        "stops": "0"
    },
    ...
]
```
